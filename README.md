# Thoast

An Android Java Library to give some style to your Toast. Inspired by [Toasty][1] and [others][2].


### Android Toast with themes

The fastest way to color a toast is to use the [theme setting][3] for doing so:

```xml
<style name="appTheme" parent="@android:styles/Theme.Holo">
  <item name="android:toastFrameBackground">@android:drawable/my_toast</item>
</style>
```

But you want more, right?


### Prerequisites

Add this in your root `build.gradle` file:

```
allprojects {
	repositories {
		...
		maven { url "https://jitpack.io" }
	}
}
```


### Dependency

Add this to your app (or module's) `build.gradle` file:

```
dependencies {
	...
	implementation 'com.gitlab.Kornelis:Thoast:0.1.0'
}
```
 

### How to use:

Use like you would a normal [Toast][4]:


```java
Thoast.makePrimary(context, "Toast message", Toast.LENGTH_SHORT).show();
```

The three fast options are: (and will throw an `Resources.NotFoundException` if the color is not set in your app's style.xml)

```java
.makePrimary(Context context, String message, int duration)
.makeSecondary(Context context, String message, int duration)
.makeAccent(Context context, String message, int duration)
```

But wait, there is even more! You can Thoast even without setting the colors in the Theme, like so:

```java
Thoast.makeThemed(context, "Message to show", int colorBackground, int colorText, Toast.DURATION_SHORT).show();
```

How to get in int color? Stackoverflow to the [rescue][5]:

Try `Color` class method:

```java
public static int parseColor(String colorString)
```


## Credits:

This App is made by Kornelis.


### Privacy notice

This library does not track or collect anything. I cannot share what is not collected.


[1]: https://github.com/GrenderG/Toasty
[2]: https://android-arsenal.com/tag/104
[3]: https://stackoverflow.com/questions/15833051/how-to-set-toast-style-as-a-theme
[4]: https://developer.android.com/guide/topics/ui/notifiers/toasts
[5]: https://stackoverflow.com/a/5248608/4421627
