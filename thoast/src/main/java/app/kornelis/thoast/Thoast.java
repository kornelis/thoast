/*
 * Copyright (C) 2021 Kornelis.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.kornelis.thoast;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.CheckResult;
import androidx.annotation.ColorInt;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;

import static android.graphics.drawable.GradientDrawable.RECTANGLE;

/**
 * @author Kornelis, February 2021.
 *
 * Let give a Toast the color of our theme: Thoast!
 *
 * This class is as simple as needed: the Toast returned can be tuned and used in the normal way.
 * That way it's use is very much like a normal Toast.
 */
public class Thoast {
    private static final int WHITE = Color.parseColor("#FFFFFF");
    private static final int BLACK = Color.parseColor("#000000");
    private static final float cornerRadius = 22f;

    /**
     * Make a Toast with the Primary color set in the style.xml.
     * @param context to use. Probably best to use getApplicationContext()
     * @param message message to show the user.
     * @param duration for how long it is visible.
     * @return a normal Toast.
     */
    @SuppressLint("ResourceType")
    @CheckResult
    @NonNull
    public static Toast makePrimary(@NonNull Context context,
                                      @NonNull CharSequence message,
                                      int duration) {

        int backgroundColor = getColorFromResId(context, R.attr.colorPrimary);
        return makeThemed(context, message, getContrastingTextColor(backgroundColor), backgroundColor, duration);
    }

    /**
     * Make a Toast with the Secondary color set in the style.xml.
     * @param context to use. Probably best to use getApplicationContext()
     * @param message message to show the user.
     * @param duration for how long it is visible.
     * @return a normal Toast.
     */
    @SuppressLint("ResourceType")
    @CheckResult
    @NonNull
    public static Toast makeSecondary(@NonNull Context context,
                                    @NonNull CharSequence message,
                                    int duration) {

        int backgroundColor = getColorFromResId(context, R.attr.colorSecondary);
        return makeThemed(context, message, getContrastingTextColor(backgroundColor), backgroundColor, duration);
    }

    /**
     * Make a Toast with the Accent color set in the style.xml.
     * @param context to use. Probably best to use getApplicationContext()
     * @param message message to show the user.
     * @param duration for how long it is visible.
     * @return a normal Toast.
     */
    @SuppressLint("ResourceType")
    @CheckResult
    @NonNull
    public static Toast makeAccent(@NonNull Context context,
                                      @NonNull CharSequence message,
                                      int duration) {

        int backgroundColor = getColorFromResId(context, R.attr.colorAccent);
        return makeThemed(context, message, getContrastingTextColor(backgroundColor), backgroundColor, duration);
    }

    /**
     * Way of directly setting the colors for the Toast.
     * @param context to use. Probably best to use getApplicationContext()
     * @param message message to show the user.
     * @param messageColor color of the text.
     * @param backgroundColor toast background color.
     * @param duration for how long it is visible.
     * @return a normal Toast.
     */
    @SuppressLint({"ShowToast", "InflateParams"})
    @CheckResult
    @NonNull
    public static Toast makeThemed(@NonNull Context context,
                                   @NonNull CharSequence message,
                                   @NonNull @ColorInt Integer messageColor,
                                   @NonNull @ColorInt Integer backgroundColor,
                                   int duration) {

        Toast toast;
        try {
            final View toastLayout = ((LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.thoast_layout, null);

            // Background
            GradientDrawable shape = new GradientDrawable();
            shape.setShape(RECTANGLE);
            shape.setColor(backgroundColor);
            shape.setCornerRadius(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cornerRadius, context.getResources().getDisplayMetrics()));
            toastLayout.setBackground(shape);

            // Text
            TextView tv = toastLayout.findViewById(R.id.thoast_text);
            tv.setText(message);
            tv.setTextColor(messageColor);

            toast = new Toast(context.getApplicationContext());
            toast.setDuration(duration);
            toast.setView(toastLayout);

        } catch (Exception e) {
            // Making sure you can see something.
            toast = Toast.makeText(context, message, duration);
        }

        return toast;
    }

    /**
     * get Text Color (Readable For Background Color)
     * @param backgroundColor to do our check on.
     * @return #WHITE or #BLACK that are readable on the background.
     */
    @ColorInt
    public static int getContrastingTextColor(@ColorInt int backgroundColor) {
        return (isDarkColor(backgroundColor)) ? WHITE : BLACK;
    }

    /**
     * Get a color from it's id in the Theme.
     * @param context needed to get to the Theme.
     * @param resId id for the color, something like: colorPrimary
     * @throws Resources.NotFoundException if no resource is found with the provided ID.
     * @return int with the color.
     */
    @ColorInt
    public static int getColorFromResId(@NonNull Context context, @IdRes int resId) throws Resources.NotFoundException {
        Resources.Theme theme = context.getTheme();
        TypedValue value = new TypedValue();
        if (!theme.resolveAttribute(resId, value, true)) {
            throw new Resources.NotFoundException("Resource NOT found, resource entry name: " + context.getResources().getResourceEntryName(resId));
        }
        return value.data;
    }

    public static boolean isDarkColor(@ColorInt int color) {
        return ColorUtils.calculateLuminance(color) < 0.5;
    }
}
